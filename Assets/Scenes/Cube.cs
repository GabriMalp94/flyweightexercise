﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    private CubeSettings _cubeSetting => SettingManager.Instance.CubeSettings;
    private MeshRenderer _meshRenderer;

    private Material CubeMaterial => _cubeSetting.CubeMaterial;
    private Vector3 DefaultSize => _cubeSetting.DefaultSize;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        _meshRenderer.material = _cubeSetting.CubeMaterial;
    }

    private void Update()
    {
        transform.localScale =  _cubeSetting.DefaultSize * _cubeSetting.SizeMultiplier;
    }
}
