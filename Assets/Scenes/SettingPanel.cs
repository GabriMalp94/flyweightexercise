﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPanel : MonoBehaviour
{
    [SerializeField] private Slider _sizeSlider;
    [SerializeField] private Slider _colorSlider;


    public void SizeUpdate()
    {
        Debug.Log(_sizeSlider.value);
        SettingManager.Instance.SetSizeMulti(_sizeSlider.value);
    }

    public void ColorLerpUpdate()
    {
        SettingManager.Instance.SetColorLerp(_colorSlider.value);
    }
}
