﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingManager : MonoBehaviour
{
    public static SettingManager Instance { get; private set; }

    [SerializeField] CubeSettings _cubeSettings;

    public CubeSettings CubeSettings => _cubeSettings;

    private float ColorLerp
    {
        get => _cubeSettings.LerpColor;
        set => _cubeSettings.LerpColor = value;
    }
    private Material CubeMaterial => _cubeSettings.CubeMaterial;
    private Color Color1 => _cubeSettings.Color1;
    private Color Color2 => _cubeSettings.Color2;

    private float SizeMultiplier
    {
        get => _cubeSettings.SizeMultiplier;
        set
        {
            _cubeSettings.SizeMultiplier = value;
            if(_cubeSettings.SizeMultiplier < 1)
            {
                _cubeSettings.SizeMultiplier = 1;
            }
        }
    }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            return;
        }
        Destroy(this.gameObject);
        MaterialUpdate();
    }

    public void SetSizeMulti(float size)
    {
        SizeMultiplier = size;
        Debug.Log(size);
    }
    public void SetColorLerp(float lerp)
    {
        ColorLerp = lerp;
        MaterialUpdate();
    }

    private void MaterialUpdate()
    {
        CubeMaterial.color = Color.Lerp(Color1, Color2, ColorLerp);
    }
}
