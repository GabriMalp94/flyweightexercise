﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "custom/CubeSetting", fileName = "Cube Settings")]
public class CubeSettings : ScriptableObject
{
    public float SizeMultiplier = 1;
    public float LerpColor = 0;
    public Color Color1;
    public Color Color2;
    public Material CubeMaterial;
    public Vector3 DefaultSize = Vector3.one;
}
